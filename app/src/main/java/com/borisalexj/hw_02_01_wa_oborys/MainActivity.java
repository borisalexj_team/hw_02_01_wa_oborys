package com.borisalexj.hw_02_01_wa_oborys;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class MainActivity extends AppCompatActivity implements UnChecker {
    private final String TAG = "hw_02_01_wa_oborys" + this.getClass().getSimpleName();
    private android.support.v4.app.FragmentManager fragmentManager;
    private CheckBox redCheckBox;
    private CheckBox greenCheckBox;
    private CheckBox blueCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        redCheckBox = (CheckBox) findViewById(R.id.red_checkBox);
        greenCheckBox = (CheckBox) findViewById(R.id.green_checkBox);
        blueCheckBox = (CheckBox) findViewById(R.id.blue_checkBox);

        redCheckBox.setOnCheckedChangeListener(createCheckerListener("red", ContextCompat.getColor(this, R.color.colorRed)));
        greenCheckBox.setOnCheckedChangeListener(createCheckerListener("green", ContextCompat.getColor(this, R.color.colorGreen)));
        blueCheckBox.setOnCheckedChangeListener(createCheckerListener("blue", ContextCompat.getColor(this, R.color.colorBlue)));
    }


    private CompoundButton.OnCheckedChangeListener createCheckerListener(final String tag, final int color) {
        CompoundButton.OnCheckedChangeListener checkerListener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                fragmentManager = getSupportFragmentManager();
                if (!isChecked) {
                    android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag(tag);
                    fragmentManager.beginTransaction().remove(fragment)
                            .commit();
                } else {
                    Fragment newFragment = new CommonFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("tag", tag);
                    bundle.putInt("color", color);
                    newFragment.setArguments(bundle);
                    if (fragmentManager.findFragmentByTag(tag) == null) {
                        fragmentManager.beginTransaction()
                                .add(R.id.main_for_fragments_layout, newFragment, tag)
                                .addToBackStack(tag)
                                .commit();
                    }
                }
            }
        };
        return checkerListener;
    }

    @Override
    public void unCheck(String tag) {
        switch (tag) {
            case "red":
                redCheckBox.setChecked(false);
                break;
            case "green":
                greenCheckBox.setChecked(false);
                break;
            case "blue":
                blueCheckBox.setChecked(false);
                break;
        }

    }
}
