package com.borisalexj.hw_02_01_wa_oborys;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by user on 3/15/2017.
 */

public class CommonFragment extends Fragment {
    private final String TAG = "hw_02_01_wa_oborys" + this.getClass().getSimpleName();
    private String fragmentTag;
    private int backgroundColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common, container, false);
        fragmentTag = getArguments().getString("tag");
        backgroundColor = getArguments().getInt("color");

        view.findViewById(R.id.fragment_background).setBackgroundColor(backgroundColor);
        Log.d(TAG, "onCreateView: " + fragmentTag);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((UnChecker) getActivity()).unCheck(fragmentTag);
        Log.d(TAG, "onDestroy: " + fragmentTag);
    }

}
